\prompt{(Advanced Problem) A spherical conductor of radius $a$ is immersed in a
uniform plasma and charged to a potential $\phi_0$.  The electrons remain
Maxwellian and move to form a Debye shield, but the ions are stationary during
the time frame of the experiment.}

\prompt{(a) Assuming $e\phi/KT_e \ll 1$, write Poisson's equation for this problem
in terms of $\lambda_D$.}

We will be following along with the example in the book.  The only difference
being that we're dealing with a spherical coordinate space ($r$, $\theta$, and
$\varphi$ coordinates), rather than a 1D linear coordinate space.  So Equation
1.12 in the book becomes:

\[
  \varepsilon_0 \laplacian \phi = \varepsilon_0 \qty(\frac1{r^2} \pdv{r}(r^2 \pdv{\phi}{r}) + \frac1{r^2 \sin^2 \varphi} \pdv[2]{\phi}{\theta} + \frac1{r^2 \sin \varphi} \pdv{\varphi}(\sin \varphi \pdv{\phi}{\varphi}))
\]

Fortunately, because the sphere is uniformly charged, we can expect $\phi(r,
\theta, \varphi) = \phi(r)$, so we have

\[
  \varepsilon_0 \laplacian \phi = \varepsilon_0 \frac1{r^2} \pdv{r}(r^2 \pdv{\phi}{r})
\]

because $\pdv{\phi}{\theta} = \pdv{\phi}{\varphi} = 0$.  This yields

\begin{equation}\leqn{spherePoisson}
  \varepsilon_0 \frac1{r^2} \pdv{r}(r^2 \pdv{\phi}{r}) = -e(n_i - n_e)
\end{equation}

If the density far away is $n_\infty$, we have

\[
  n_i = n_\infty
\]

In the presence of a potential energy $q\phi$, the electron distribution
function is

\[
  f(u) = A \exp[-(\tfrac1{2} mu^2 + q\phi)/KT_e]
\]

Integrating $f(u)$ over $u$, setting $q = -e$, and noting that $n_e(\phi \to
0) = n_\infty$, we find exactly as the book

\[
  n_e = n_\infty \exp(e\phi/KT_e)
\]

Substituting into equation \reqn{spherePoisson}, and then performing the same
steps as in the book.
 
\begin{align*}
  \varepsilon_0 \frac1{r^2} \pdv{r}(r^2 \pdv{\phi}{r}) &= e n_\infty \qty(\E{e\phi/KT_e} - 1) \\
  \varepsilon_0 \frac1{r^2} \pdv{r}(r^2 \pdv{\phi}{r}) &= e n_\infty \qty(\frac{e\phi}{KT_e} + \frac1{2}{\qty(\frac{e\phi}{KT_e})}^2 + \cdots) \\
  \varepsilon_0 \frac1{r^2} \pdv{r}(r^2 \pdv{\phi}{r}) &= \frac{n_\infty e^2}{KT_e} \phi
\end{align*}

Next, we can use the definition of the Debye length to combine our stray
constants.

\[
  \frac1{r^2} \pdv{r}(r^2 \pdv{\phi}{r}) = \frac1{\lambda_D^2} \phi
\]

This is the Poisson equation for this problem in terms of $\lambda_D$.

\prompt{(b) Show that the equation is satisfied by the function of the form
$\E{-kr}/r$. Determine $k$ and derive an expression for $\phi(r)$ in terms of
$a$, $\phi_0$, and $\lambda_D$.}

Because we're given an ansatz, we can simply substitute and simplify.

\begin{align*}
  \frac1{r^2} \pdv{r}(r^2 \pdv{\phi}{r}) &= \frac1{\lambda_D^2} \phi \\
  \frac1{r^2} \pdv{r}(r^2 \pdv{r}(\E{-kr}{r})) &= \frac{\E{-kr}}{r\lambda_D^2} \\
  \pdv{r}(r^2 \pdv{r}(\E{-kr}{r})) &= \frac{r \E{-kr}}{\lambda_D^2} \\
  \pdv{r}(r^2 \qty(- \frac{(r + k)\E{-kr}}{r^2})) &= \frac{r \E{-kr}}{\lambda_D^2} \\
  -\pdv{r}((r + k)\E{-kr}) &= \frac{r \E{-kr}}{\lambda_D^2} \\
  k^2 r \E{-kr} &= \frac{r \E{-kr}}{\lambda_D^2}
\end{align*}

From this we can tell that that $k = \tfrac1{\lambda_D}$.

At this point we must also account for the finite size of the sphere and the
potential on the surface of the sphere.

In order to place this charge it will be assumed that the charge will be placed
on an infinitesimally thin shell located at $r = a$.  As a reminder, the charge
within a charged conducting object (in steady state) is 0, so the potential
within the sphere is held constant at $\phi_0$.

We will be using the boundary condition that $\phi(a) = \phi_0$.  The leading
constant $A$ is because our ansatz solution always has the possibility to have a
leading constant, (we can see immediately that including such a constant would
also satisfy the Poisson equation as well, and, in fact, determining this
constant is the entire purpose of this part of the problem.)

\begin{align*}
  \phi(a) &= \phi_0 \\
  \phi_0 &= A \E{-a/\lambda_D}/a \\
  A &= a \phi_0 \E{a/\lambda_D}
\end{align*}

Therefore, our final equation of $\phi(r)$ is given by the piecewise equation

\[
  \phi(r) =
    \begin{cases}
      \phi_0 & r \le a \\
      \phi_0 a \E{-(r - a)/\lambda_D}/r & r > a
    \end{cases}.
\]
