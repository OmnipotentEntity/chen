\prompt{A field-effect transistor (FET) is basically an electron valve that
operates on a finite-Debye-length effect. Conduction electrons flow from the
source S to the drain D through a semiconducting material when a potential is
applied between them. When a negative potential is applied to the insulated gate
G, no current can flow through G, but the applied potential leaks into the
semiconductor and repels electrons, The channel width is narrowed and the
electron flow impeded in proportion to the gate potential. If the thickness of
the device is too large, Debye shielding prevents the gate voltage from
penetrating far enough. Estimate the maximum thickness of the conduction layer
of an n-channel FET if it has doping level (plasma density) of
\SI{1e22}{m^{-3}}, is at room temperature, and is to be no more than 10 Debye
lengths thick.}

We assume that the potential applied to the gate and the source are the same
magnitude.

First, let's find the Debye length.

\begin{align*}
  \lambda_D &= 69 {(T_e/n)}^{1/2} \\
  &= 69 \times {(293.2 / \num{1e22})}^{1/2} \\
  &= \SI{1.18e-8}{m} \\
  &= \SI{11.8}{nm}
\end{align*}

From the figure, we assume that the gate is situated halfway between the source
and drain and does not have significant thickness.  As a result, we can assume
that the kinetic energy of the electrons is increased by approximately half of
the gate's potential.  Using the result of Problem \rprob{1}{6}, we can find the
distance required to make the potential between the gates equal to the expected
energy of the electrons from the source when taking into account their average
kinetic energy plus the energy gain from traveling half of the distance from
source to drain.  This amount is

\[
  \frac{KT + q\phi}{2}.
\]

Where $KT/2$ is the average kinetic energy in one dimension as function of the
temperature and $\phi/2$ is half the gate potential.  We will be evaluating the
equation from Problem \rprob{1}{6} at $x = 0$.  Further, recall that we want the
potential at the midpoint to be equal to the expected total kinetic energy of
the particle at this point, so we have $q\phi = (KT + q\phi)/2$, or $KT =
q\phi$.

\begin{align*}
  \phi &= \frac{nq}{2 \varepsilon_0} d^2 \\
  q\phi &= \frac{nq^2}{2 \varepsilon_0} d^2 \\
  KT &= \frac{nq^2}{2 \varepsilon_0} d^2 \\
  d &= \sqrt{\frac{2 \varepsilon_0 KT}{nq^2}}
\end{align*}

This form should look very familiar, in fact it is simply $\sqrt{2} \lambda_D$.

Recall that, in Problem \rprob{1}{6}, $d$ represented the distance from the plate
to the center line, so the total thickness is twice this. Thus our final
thickness is $2 \sqrt{2} \lambda_D = \SI{33.4}{nm}$.
