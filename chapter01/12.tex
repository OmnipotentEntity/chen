\prompt{(Advanced Problem) Ionization is caused by electrons in the tail of a
Maxwellian distribution which have energies exceeding the ionization potential.
For instance, this potential is $E_{\text{ioniz}} = \SI{15.8}{eV}$ in argon.
Consider a one-dimensional plasma with electron velocities $u$ in the $x$
direction only. What fraction of the electrons can ionize for given $KT_e$ in
argon? (Give an analytic answer in terms of error functions.)}

For brevity, I will let $E_0 = E_{\text{ioniz}}$.

Per the result of Problem \rprob{1}{2}, the Maxwellian distribution in one
dimension is given by

\[
  {\qty(\frac{m}{2\pi KT_e})}^{1/2} \exp(-mu^2/2KT_e).
\]

As a reminder, because this distribution has now been normalized it is a
probability distribution function (PDF), so to find the fraction of electrons
that can ionize for a given $KT_e$, we need to find the integral of the areas of
the PDF with magnitude greater than $E_0$.  Because this function is even and
because the PDF is normalized, we can instead calculate twice the integral from
$0$ to $E_0$ and then subtract it from one.  This might seem like we're
overcomplicating things, this way of looking at the problem makes it much easier
to use the error function.

\[
  \erf z = \frac{2}{\sqrt{\pi}} \pint{0}{z}{\E{-t^2}}{t}
\]

Let's get started.  If we let $I(KT_e)$ be the function that gives the
ionization fraction from the temperature, then we have:

\[
  I(KT_e) = 1 - 2 \pint{0}{E_0}{{\qty(\frac{m}{2\pi KT_e})}^{1/2} \exp(-mu^2/2KT_e)}{u}
\]

Let's consider just the integral for a little bit.  Taking $t^2 = mu^2/2KT$, we
can make the following substitutions.

\begin{align*}
  t^2 &= mu^2/2KT & u &= E_0  \\
  \dd{t} &= \sqrt{m/2KT} \dd{u} & t &= \sqrt{2KT/m} E_0 = E_1
\end{align*}

Giving the integral

\begin{align*}
  \pint{0}{E_0}{{\qty(\frac{m}{2\pi KT_e})}^{1/2} \exp(-mu^2/2KT_e)}{u} &= 
  \frac1{\sqrt{\pi}} \pint{0}{E_1}{\E{-t^2}}{t} \\
  &= \frac1{2} \erf(E_1).
\end{align*}

Finally,

\begin{align*}
  I(KT_e) &= 1 - 2 \cdot \frac1{2} \erf(E_1) \\
  &= 1 - \erf\qty(\sqrt{2KT/m} E_0).
\end{align*}
