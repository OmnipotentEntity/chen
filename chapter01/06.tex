\prompt{An alternative derivation of $\lambda_D$ will give further insight to
its meaning. Consider two infinite parallel plates at $x = \pm d$, set at
potential $\phi = 0$. The space between them is uniformly filled by a gas of
density $n$ of particles of charge $q$.}

\prompt{(a) Using Poisson's equation, show that the potential distribution
between the plates is}

\[
  \phi = \frac{nq}{2 \varepsilon_0} (d^2 - x^2)
\]

Because we have infinite parallel plates, by symmetry this is simply a 1D
problem and we can use the one dimensional version of Poisson's equation.

\[
  \varepsilon_0 \dv[2]{\phi}{x} = nq
\]

Next, we can simply integrate twice and use reasonable boundary conditions to
select the integration constants.

\begin{align*}
  \iint \varepsilon_0 \dv[2]{\phi}{x} \dd{x} \dd{x} &= \iint -nq \dd{x} \dd{x} \\
  \int \varepsilon_0 \dv{\phi}{x} \dd{x} &= \int -nqx + C \dd{x} \\
  \varepsilon_0 \phi &= -\frac{nq}{2}x^2  + Cx + D \\
  \phi &= -\frac{nq}{2 \varepsilon_0}x^2  + Cx + D
\end{align*}

From here, it is easy to see that we can get the equation we want by setting $C
= 0$ and $D = \frac{nqd^2}{2 \varepsilon_0}$.  What should these values actually
be physically?

Well, due to the symmetry of the problem, we require that $\phi(d) = \phi(-d)$,
so clearly $C = 0$ (otherwise they would differ by $2Cd$).  However, due to
$\phi$ being an electric potential, we don't have a defined 0 point, so we are
actually free to set this 0 point wherever we find it convenient.  Setting this
0 value at $x = d$ we can obtain the value required.

\prompt{(b) Show that for $d > \lambda_D$, the energy needed to transport a
particle from a plate to the midplane is greater than the average kinetic energy
of the particles.}

To restate, the Debye length is given by:

\[
  \lambda_D \equiv {\qty(\frac{\varepsilon_0 KT}{nq^2})}^{1/2}
\]

Simply substituting $d = \lambda_D$ into the equation and computing at the
maximum at $x = 0$ we arrive at

\begin{align*}
  \phi &= \frac{nq}{2 \varepsilon_0} \qty({\qty({\qty(\frac{\varepsilon_0 KT}{nq^2})}^{1/2})}^2 - 0) \\
  &= \frac{nq}{2 \varepsilon_0} \times \frac{\varepsilon_0 KT}{nq^2} \\
  &= \frac{KT}{2q} \\
  q \phi &= \frac{KT}{2}
\end{align*}

On the left we have a potential energy, and on the right we have a kinetic
energy.

The average kinetic energy in one dimension (even though this is a three
dimensional problem, only one dimension actually matters to this problem,
because only one direction represents travel along the potential) is given by
$KT/2$, which is the value we arrived at.

By inspection, if we use a value of $d$ larger than $\lambda_D$ the kinetic
energy required to move to this potential is higher; therefore, an average
particle could not move to this position.
