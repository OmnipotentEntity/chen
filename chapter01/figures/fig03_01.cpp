#include <cmath>
#include <iostream>

// Calculates the x-coord assuming n total points and even spacing on a
// logarithmic axis
double logspace(int a, int n, double min, double max) {
  double log_diff = log2(max/min); // log2 is fast, and scale doesn't matter
  double log_spacing = log_diff/(n-1);
  return min * exp2(a * log_spacing);
}

double const_debye_curve(double n, double lambda_d) {
  const double epsilon_0 = 5.52634936e7; // Units e^2/(eV m), e^2 cancels in the
                                         // formula
  return lambda_d * n / epsilon_0;
}

double const_nd_curve(double n, double n_d) {
  const double nd_const = 6.971e-9;

  return nd_const * pow(n, 1./3.) * pow(n_d, 2./3.);
}

const double xmin = 1e6;
const double xmax = 1e28;

const double ymin = 1e-2;
const double ymax = 1e5;

void drawtext() {
  int i,n;
  double x,y;

  TText t;
  t.SetTextAlign(13);
  t.SetTextColor(kBlue);
  t.SetTextFont(43);
  t.SetTextSize(20);

  auto mg = (TMultiGraph*)gPad->GetPrimitive("Multigraph");
  auto g = (TGraph*)mg->GetListOfGraphs()->FindObject("Points");

  n = g->GetN();

  const char * labels[] = {"Reactor", "Experiment (torus)", "Experiment (pinch)", "Ionosphere", "Radiofreq plasma", "Flame", "Laser", "Space"};

  for (i=0; i<n; i++) {
    g->GetPoint(i,x,y);
    t.PaintText(log10(x), log10(y), Form(" %s", labels[i]));
  }
}

void fig03_01() {
  auto cv = new TCanvas("fig03_01", "fig03_01", 1920, 1200);
  cv->SetLogx();
  cv->SetLogy();
  cv->SetGrid();

  auto *mg = new TMultiGraph();
  mg->SetName("Multigraph");

  const int graph_points = 10001;

  const int lambda_graph_count = 5;
  double lambda_d[lambda_graph_count] = { 1, 1e-2, 1e-4, 1e-6, 1e-8 };
  const char *lambda_d_str[lambda_graph_count] = { "#lambda_{D} = 1 m", "#lambda_{D} = 10^{-2} m", "#lambda_{D} = 10^{-4} m", "#lambda_{D} = 10^{-6} m", "#lambda_{D} = 10^{-8} m" };

  const int nd_graph_count = 3;
  double n_d[nd_graph_count] = {1e3, 1e6, 1e9};
  const char *nd_str[nd_graph_count] = {"N_{D} = 10^{3} m^{-3}", "N_{D} = 10^{6} m^{-3}", "N_{D} = 10^{9} m^{-3}"};

  TGraph* g_debye_length[lambda_graph_count];
  TGraph* g_debye_sphere[nd_graph_count];
  

  // debye length
  for (int i = 0; i < lambda_graph_count; ++i) {
    g_debye_length[i] = new TGraph();
    for (int j=0; j < graph_points; ++j) {
      double x_coord = logspace(j, graph_points, xmin, xmax);
      g_debye_length[i]->AddPoint(x_coord, const_debye_curve(x_coord, lambda_d[i]));
    }
    g_debye_length[i]->SetLineColor(i+40);
    g_debye_length[i]->SetTitle(lambda_d_str[i]);
    mg->Add(g_debye_length[i], "l");

  }

  // debye sphere
  for (int i = 0; i < nd_graph_count; ++i) {
    g_debye_sphere[i] = new TGraph();
    for (int j=0; j < graph_points; ++j) {
      double x_coord = logspace(j, graph_points, xmin, xmax);
      g_debye_sphere[i]->AddPoint(x_coord, const_nd_curve(x_coord, n_d[i]));
    }
    g_debye_sphere[i]->SetLineColor(i+40);
    g_debye_sphere[i]->SetLineStyle(10);
    g_debye_sphere[i]->SetTitle(nd_str[i]);
    mg->Add(g_debye_sphere[i], "l");
  }

  // plasma points
  const int point_count = 8;
  const double ns[point_count] = {1e20, 1e19, 1e23, 1e11, 1e17, 1e14, 1e25, 1e6};
  const double ts[point_count] = {3e4, 100, 1000, 0.05, 1.5, 0.1, 100, 0.01};
  auto *g_points = new TGraph();
  g_points->SetName("Points");

  auto ex = new TExec("ex", "drawtext();");
  g_points->GetListOfFunctions()->Add(ex);

  g_points->SetTitle("Various Example Plasmas");
  g_points->SetMarkerStyle(20);
  for (int i = 0; i < point_count; ++i) {
    g_points->SetPoint(i, ns[i], ts[i]);
  }
  mg->Add(g_points, "P");

  mg->SetTitle("Lines of constant #lambda_{D} and N_{D};n;KT_{e}");
  mg->GetXaxis()->SetLimits(xmin, xmax);
  mg->SetMinimum(ymin);
  mg->SetMaximum(ymax);
  mg->Draw("a");

  cv->BuildLegend();
  cv->Update();
  cv->Modified();
}
